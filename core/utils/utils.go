package utils

import (
	"encoding/json"
	"fmt"
	"strings"
)

func TrimRight(text string) string {
	text = strings.TrimRightFunc(text, func(c rune) bool {
		//In windows newline is \r\n
		return c == '\r' || c == '\n'
	})

	return text
}

func PrintPretty(obj interface{}) {
	b, err := json.MarshalIndent(obj, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Println(string(b))
}
