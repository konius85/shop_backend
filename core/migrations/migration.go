package migrations

import (
	"database/sql"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/config"
)

func Up() {
	connectionString := "host=" + config.Config.Postgres.Host + " " +
		"database=" + config.Config.Postgres.Database + " " +
		"user=" + config.Config.Postgres.User + " " +
		"password=" + config.Config.Postgres.Password + " " +
		"sslmode=disable"

	db, err := sql.Open("postgres", connectionString)

	if err != nil {
		fmt.Println("Migration error", err)
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})

	defer db.Close()

	if err != nil {
		fmt.Println("Migration error", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"postgres", driver)

	if err != nil {
		fmt.Println("Migration file", err)
	}

	err = m.Up()

	if err != nil {
		fmt.Println("UP error", err)
	}

}
