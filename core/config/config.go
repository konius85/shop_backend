package config

import (
	"fmt"
	"github.com/jinzhu/configor"
	"log"
	"os"
)

var Config struct {
	AppPort int `default:"8282"`

	Postgres struct {
		Host     string `default:"db"`
		User     string `default:"root"`
		Database string `default:"workflow"`
		Password string `default:"root"`
		Port     int    `default:"5432"`
	}

	MaxUploadFileSize int    `default:"20"`
	FilePath          string `default:"./file"`
}

func LoadConfig() {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fileName := dir + "/config/config.yml"
	fmt.Println("FILE NAME", fileName)

	configor.Load(&Config, fileName)

	//utils.PrintPretty(&Config)
	if err != nil {
		log.Fatal(err)
	}
}
