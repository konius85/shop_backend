package models

import (
	"fmt"
	"time"
)

type Document struct {
	Id           int    `pg:",pk"`
	Name         string `pg:",notnull"`
	Desc         string
	IsDel        bool      `pg:"default:false"`
	CreatedAt    time.Time `pg:"default:now()"`
	UpdatedAt    time.Time `pg:",use_zero"`
	DocumentType *DocumentType
}

func (d Document) String() string {
	return fmt.Sprintf("Document<%v %s %v %t %v %s %s>", d.Id, d.Name, d.Desc, d.IsDel, d.DocumentType, d.CreatedAt, d.UpdatedAt)
}
