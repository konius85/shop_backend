package models

import "fmt"

type Product struct {
	Id         int64 `pg:",pk"`
	Name       string
	IsDel      bool `pg:"default:false"`
	PriceNet   float64
	PriceGross float64
	Quantity   float64
	Unit       *Unit
}

func (p Product) String() string {
	return fmt.Sprintf("DocumentType<%d %s %t>", p.Id, p.Name, p.IsDel)
}
