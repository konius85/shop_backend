package models

import (
	"fmt"
	"time"
)

type File struct {
	Id            int64
	DocId         int64
	OriginalName  string
	Name          string `pg:",notnull"`
	Desc          string
	IsDel         bool      `pg:"default:false"`
	CreatedAt     time.Time `pg:"default:now()"`
	UpdatedAt     time.Time `pg:",use_zero"`
	Extension     string    `pg:"type:varchar(25)"`
	Size          int64
	Path          string
	Directory     string
	MimeType      string
	CreatedUserId int
	CreatedUser   *User
	UpdatedUserId int
	UpdatedUser   *User
}

func (f File) String() string {
	return fmt.Sprintf("File<%d %s %v %t %d %s %s>", f.Id, f.Name, f.Desc, f.IsDel, f.CreatedAt, f.UpdatedAt)
}
