package document

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/services/dbService"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/file"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/utils"
)

func Create(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	w.Header().Set("Content-Type", "text/json")
	var Data models.Document

	err := json.NewDecoder(r.Body).Decode(&Data)
	err = con.Insert(&Data)
	err = json.NewEncoder(w).Encode(Data)
	if err != nil {
		log.Println(err)
		return
	}

	fileData, err := file.UploadFile(w, r)
	if err != nil {
		log.Println(err)
		return
	}

	fileData.DocId = int64(Data.Id)

	utils.PrintPretty(fileData)
	err = con.Insert(fileData)

	if err != nil {
		log.Println(err)
		return
	}
}

func Update(w http.ResponseWriter, r *http.Request) {

}

func Delete(w http.ResponseWriter, r *http.Request) {

}

func GetAll(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	var documents = make([]models.Document, 0)
	err := con.Model(&documents).Select()
	if err != nil {
		panic(err)
	}

	type respond struct {
		Hits []models.Document `json:"hits"`
	}

	res := respond{
		Hits: documents,
	}
	err = json.NewEncoder(w).Encode(res)
	if err != nil {
		panic(err)
	}
}
