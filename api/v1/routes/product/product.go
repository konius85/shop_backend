package product

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/services/dbService"
)

func Create(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	w.Header().Set("Content-Type", "text/json")
	var Data models.Product

	err := json.NewDecoder(r.Body).Decode(&Data)
	err = con.Insert(&Data)
	err = json.NewEncoder(w).Encode(Data)
	if err != nil {
		log.Println(err)
		return
	}

	err = json.NewEncoder(w).Encode(Data)
	if err != nil {
		panic(err)
	}
}

func Update(w http.ResponseWriter, r *http.Request) {

}

func Delete(w http.ResponseWriter, r *http.Request) {

}

func GetAll(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	var products = make([]models.Product, 0)
	err := con.Model(&products).Select()
	if err != nil {
		panic(err)
	}

	type respond struct {
		Products []models.Product `json:"products"`
	}

	res := respond{
		Products: products,
	}
	err = json.NewEncoder(w).Encode(res)
	if err != nil {
		panic(err)
	}
}
