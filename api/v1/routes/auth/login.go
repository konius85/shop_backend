package auth

import (
	"crypto/ed25519"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/o1egl/paseto"
	"log"
	"net/http"
	"time"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/services/dbService"
)

type Person struct {
	Email    string
	Password string
}

type response struct {
	UserId int    `json:"userId"`
	Token  string `json:"token"`
}

type tokenData struct {
	Token string
}

var p paseto.V2

func Login(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	var p Person
	err := json.NewDecoder(r.Body).Decode(&p)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var user models.User
	err = con.Model(&user).Where("email = ?", p.Email).Where("is_del = ?", false).Select()
	if err != nil {
		http.Error(w, "Authorization field. User not exists", http.StatusUnauthorized)
		return
	}
	w.Header().Set("Content-Type", "application/json")

	token := createToken(user.Id)

	var res = response{
		UserId: user.Id,
		Token:  token,
	}
	err = json.NewEncoder(w).Encode(&res)
	if err != nil {
		log.Println(err)
		return
	}
}

func createToken(userId int) string {
	b, _ := hex.DecodeString("b4cbfb43df4ce210727d953e4a713307fa19bb7d9f85041438d9e11b942a37741eb9dbbbbc047c03fd70604e0071f0987e16b28b757225c11f00415d0e20b1a2")
	privateKey := ed25519.PrivateKey(b)

	// or create a new keypair
	// publicKey, privateKey, err := ed25519.GenerateKey(nil)

	jsonToken := paseto.JSONToken{
		Expiration: time.Now().Add(24 * time.Hour),
	}

	// Add custom claim    to the token
	jsonToken.Set("id", string(userId))
	footer := "some footer"

	// Sign data
	token, err := p.Sign(privateKey, jsonToken, footer)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("token", token)
	return token
}

func verifyToken(token string) {
	b, _ := hex.DecodeString("1eb9dbbbbc047c03fd70604e0071f0987e16b28b757225c11f00415d0e20b1a2")
	publicKey := ed25519.PublicKey(b)

	var newJsonToken paseto.JSONToken
	var newFooter string
	err := p.Verify(token, publicKey, &newJsonToken, &newFooter)
	fmt.Println(newJsonToken, newFooter)
	// Verify data
	if err != nil {
		log.Println(err)
	}
}
