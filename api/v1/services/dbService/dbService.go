package dbService

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/database"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/database/postgres"
)

func PgConnection() *pg.DB {
	return postgres.PgConnect()
}

func CreateSchema(models []interface{}) error {
	conn := database.T{}
	db := conn.PgConnect()
	defer db.Close()

	for _, model := range models {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			Temp:          false,
			IfNotExists:   true,
			FKConstraints: true,
			Varchar:       255,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
