CREATE OR REPLACE FUNCTION addUserRole(roleName text) RETURNS integer AS $$
DECLARE userId int;
    BEGIN
     INSERT INTO user_roles (name) VALUES ($1) RETURNING id INTO userId;
     return userId;
END;
$$ LANGUAGE plpgsql;


INSERT INTO users (f_name, l_name, email, password, user_role_id)
VALUES ('admin', 'admin', 'admin@admin.pl', 'admin', addUserRole('ADMIN'))


