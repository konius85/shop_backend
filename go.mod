module gitlab.com/konius85/delivery-workspace/shop-order-backend

go 1.14

require (
	github.com/go-pg/pg/v9 v9.1.6
	github.com/go-pg/urlstruct v0.4.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.11.0
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/configor v1.2.0
	github.com/lib/pq v1.7.0
	github.com/o1egl/paseto v1.0.0
	github.com/rs/cors v1.7.0
	github.com/segmentio/encoding v0.1.11 // indirect
	github.com/vmihailenco/bufpool v0.1.11 // indirect
	github.com/vmihailenco/msgpack/v4 v4.3.11 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/grpc v1.29.1 // indirect
)
