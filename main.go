package main

import (
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/routes"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/config"
	"log"
	"net/http"
)

func init() {
	config.LoadConfig()
}

func handleRequest() {
	router := mux.NewRouter().StrictSlash(true)
	routes.RegisterRoutes(router)

	appPort := fmt.Sprint(config.Config.AppPort)
	log.Println("Server is running and listening on port " + appPort)
	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	allowedMethod := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"})
	allowedOrigins := handlers.AllowedOrigins([]string{"http://localhost:3000"})

	log.Fatal(http.ListenAndServe(":"+appPort, handlers.CORS(allowedHeaders, allowedMethod, allowedOrigins)(router)))
}

func main() {
	handleRequest()
}
